module.exports.iniciaChat = function(application, req, res){
  var dadosForm = req.body;

//validando os dados que são digitados no fomulario
  req.assert('apelido', 'Apelido Obrigatório').notEmpty();
  req.assert('apelido', 'Apelido deve conter entre 3 e 10 caracteres').len(3,10);

  //função para retornar os errosna validação
  var erros = req.validationErrors();

  if(erros){
    res.render('index', {validacao: erros});
    return;
  }
  // recuperando a variavel io da intancial do express
  application.get('io').emit(
    'msgParaCliente',
    {apelido: dadosForm.apelido, mensagem: 'Acabou de entrar'}
  );

  res.render('chat');
}
