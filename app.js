/*Importar as configurações do servidor*/
const app = require('./config/server');

// PARAMETRIZAR A PORTA DE ESCUTA
const server = app.listen(80, function(){
  console.log("SERVER RUNNING ACCESS 127.0.0.1 IN YOUR BROWSER");
})

// require do socket.io para que também seja interpretado pela porta 80 do navegador.
let io = require('socket.io').listen(server);
// criando uma variaveil global para ser usar no chat.js sem que ela recebe por paramentro na function
app.set('io', io);

// criar a conexão por Websockets
io.on('connection', function(socket){
  console.log("User conectado");
  // Podemos criar varios eventos de escuta emissão, etc
  socket.on('disconnect', function(){
    console.log("User desconec");
  });
  socket.on('msgParaServidor', function(data){
     socket.emit('msgParaServidor',
     {apelido: data.apelido, mensagem: data.mensagem});
  });

});
