// IMPORTAR O MODULO DO FRAMEWORK EXPRESS
var express = require('express');
// IMPORTAR O MODULO DO CONSIGN PARA NOSSAS ROTAS
var consign = require('consign');
// IMPORTAR O MODULO DO BODY-PARSER PARA O RECUPERAR OS ENVIOS DAS INFORMAÇOES DOS FOMULARIOS VIA POST COMO JSON
var bodyParser = require('body-parser');
// IMPORTAR O MODULO DO EXPRESS VALIDATOR PARA VALIDAÇÃO DOS DADOS QUE SÃO DIGITADOS NOS FOMULARIOS
var expressValidator = require('express-validator');

// INICIAR OBJ DO EXPRESS
var app = express();
// SETAR AS VARIAVEIS QUE AS 'VIEW ENGINE', 'VIEWS' DO EXPRESS E O LOCAL
app.set('view engine', 'ejs');
app.set('views', './app/views');
// CONFIGURAR O MIDDLEWARES EXPRESS.STATIC
app.use(express.static('./app/public'));
// CONFIGURAR O MIDDLEWARES BODY-PARSER
app.use(bodyParser.urlencoded({extend: true}));
// CONFIGURAR O MIDDLEWARES EXPRESS-VALIDATOR
app.use(expressValidator());
// CONFIGURAR O MIDDLEWARES CONSIGN PARA AUTO CARREGAR  ROTAS DOS MODELS, CONTROLLERS PARA O OBJ APP
consign()
  .include('app/routes')
  .then('app/models')
  .then('app/controllers')
  .into(app)

// EXPORTAR O OBJ APP
module.exports = app;
